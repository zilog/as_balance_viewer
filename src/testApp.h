#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxTimeline.h" //add timeline include
#include "ofxTLAudioTrack.h"

#define MAX_TRAIL_PTS 200

// listen on port 12345
#define PORT 12345
#define NUM_MSG_STRINGS 20

#define SCALE_WEIGHT 100

enum eSpeechState {
	SPEECH_WAITING,
	SPEECH_IN_PROCESS,
	SPEECH_INTERRUPTED
};

enum ePose {
	POSE_NONE,
	POSE_SITTING,
	POSE_STANDING,
	POSE_BOUNCING
};

class testApp : public ofBaseApp{

public:
    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

		void speechStart();
		void speechResume();
		void speechEnd();
		void speechInterrupt();
		void updateSpeechState();
		string getStateString();
		
		bool inSafeArea();
		string getPoseString();
		ePose getCurrentPose(float weightReading);

		void playbackEnded(ofxTLPlaybackEventArgs& pbe);
		void receivedBang(ofxTLBangEventArgs& bang);
    void updateOscQueue();

    deque<ofPoint> trail;
    ofRectangle sky;
    ofRectangle ground;
		ofxTimeline timeline;
		ofxTLAudioTrack waveform;

		ePose currentPose;
		
		ofSoundPlayer  interrupted;
		
		eSpeechState lastState;
		eSpeechState state;
		
    ofRectangle safetyArea;
		float lastWeightMeasure;
    ofPoint cursor;
		long lastPlayheadPos;
		
		ofImage slides[6];
		std::map<string, ofImage> slidemap;
		std::string currentSlide;
		
    ofFloatColor csky, chup, chdown, cground;

    bool bMouseMode;
    bool bEditMode;
		bool bShowDebuggingInfo;
		
    ofxOscReceiver receiver;
};
