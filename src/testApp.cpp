#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){

	ofSetLogLevel(OF_LOG_VERBOSE);
	// listen on the given port
	cout << "listening for osc messages on port " << PORT << "\n";
	receiver.setup(PORT);

	float whv = ofGetWidth() / 2;
	float hhv = ofGetHeight() / 2;
	sky = ofRectangle(-whv, -hhv, whv*2, hhv);
	ground = ofRectangle(-whv, 0, whv*2, hhv);

	bMouseMode = false;
	bEditMode = false;
	lastWeightMeasure = .0;
	bShowDebuggingInfo = false;
	
	state = SPEECH_WAITING;
	
	float sax = -ofGetViewportWidth() * .08;
	float say = -ofGetViewportHeight() * .08;
	this->safetyArea = ofRectangle(sax, say, ofGetViewportWidth() * .16, ofGetViewportHeight() * .16);
	
	csky = ofColor::fromHex(0x969AEF);
	chup = ofColor::fromHex(0xFFF0EC);
	cground = ofColor::fromHex(0xBC6A21);
	chdown = ofColor::fromHex(0xF0BE1D);
	
	// prepapre slides and map them to flag names in the timeline
	/*
        <flag>mancomp</flag>
        <flag>touch</flag>
        <flag>kfail</flag>
        <flag>zumba</flag>
        <flag>csomatics</flag>
	*/
	slides[0].loadImage("images/mancomp.png");
	slides[0].setAnchorPercent(0.5, 0.5); 
	slidemap["mancomp"] = slides[0];
	slides[1].loadImage("images/csomatics.jpg");
	slides[1].setAnchorPercent(0.5, 0.5); 
	slidemap["csomatics"] = slides[1];
	slides[2].loadImage("images/kinect-fail.jpg");
	slides[2].setAnchorPercent(0.5, 0.5); 
	slidemap["kfail"] = slides[2];
	slides[3].loadImage("images/zumba.jpg");
	slides[3].setAnchorPercent(0.5, 0.5); 
	slidemap["zumba"] = slides[3];
	slides[4].loadImage("images/touchscreen.jpg");
	slides[4].setAnchorPercent(0.5, 0.5); 
	slidemap["touch"] = slides[4];
	
	currentSlide = "none";
	
	// interrupted
	interrupted.loadSound("interrupted.wav");
	
	// timeline
	ofxTimeline::removeCocoaMenusFromGlut("balanceViewerDebug");
	timeline.setup(); //registers events
	timeline.addTrack("Recitation", &waveform);
	waveform.loadSoundfile("lecture-as-kinesthesia.aiff");
	timeline.setDurationInSeconds(waveform.getDuration());
	timeline.setLoopType(OF_LOOP_NONE); // don't loop, thank you

	if(bEditMode) {
		timeline.show();
	} else {
		timeline.hide();
	}

	// add a track
	timeline.addBangs("playhead");
	timeline.addFlags("slides");
	ofAddListener(timeline.events().bangFired, this, &testApp::receivedBang);
	ofAddListener(timeline.events().playbackEnded, this, &testApp::playbackEnded);
	
	ofToggleFullscreen();
}

void testApp::playbackEnded(ofxTLPlaybackEventArgs& pbe) {
	ofLogNotice() << "playback ended";
	speechEnd();
}

void testApp::receivedBang(ofxTLBangEventArgs& bang) {
  ofLogNotice("Bang fired from track " + bang.track->getName());

	if(bang.track->getName() == "playhead") {
		lastPlayheadPos = bang.currentMillis;
	} else if(bang.track->getName() == "slides"){
		  ofLogNotice("Slide flag: " + bang.flag);
			
			// search if we have a slide for this flag
			map<string, ofImage>::iterator it = slidemap.find(bang.flag);
			if(it != slidemap.end()) {
				currentSlide = bang.flag;
				ofLogNotice() << "currentSlide = " << currentSlide << endl;
			} else {
				currentSlide = "none";
			}
  } // if..else
}

string testApp::getPoseString() {
	switch(this->currentPose) {
		case POSE_NONE:
			return "NONE";
			break;
		case POSE_SITTING:
			return "SITTING";
			break;
		case POSE_STANDING:
			return "STANDING";
			break;
		case POSE_BOUNCING:
			return "BOUNCING";
			break;
		default:
			return "unknown";
			break;
	}
}

string testApp::getStateString() {
	switch(state) {
		case SPEECH_IN_PROCESS:
			return "IN PROGRESS";
			break;
		case SPEECH_INTERRUPTED:
			return "INTERRUPTED";
			break;
		case SPEECH_WAITING:
			return "WAITING";
			break;
		default:
			return "unknown";
			break;
	}
}

void testApp::speechStart() {
	waveform.play();
	timeline.play();
	state = SPEECH_IN_PROCESS;
}

void testApp::speechResume() {
	interrupted.play();
	
	while( interrupted.getIsPlaying() == true ) {
		ofSleepMillis(2000);
	}
	
	waveform.player.setPositionMS(lastPlayheadPos); //set(); //play();
	timeline.setCurrentTimeMillis(lastPlayheadPos); //play();
	
	waveform.play();
	timeline.play();
	
	state = SPEECH_IN_PROCESS;
}

void testApp::speechEnd() {
	state = SPEECH_WAITING;
}

void testApp::speechInterrupt() {
	waveform.stop();
	timeline.stop();
	state = SPEECH_INTERRUPTED;
}

ePose testApp::getCurrentPose(float weightReading) {
	ePose retval;
	
	if( (weightReading < 20) ) {
		retval = POSE_NONE;
	} else if( (weightReading > 75) && (weightReading < 95) ) {
		retval = POSE_SITTING;
	} else if( (weightReading > 100) && (weightReading < 108) ) {
		retval = POSE_STANDING;
	} else if( (weightReading > 125) ) {
		retval = POSE_BOUNCING;
	}
	
	return retval;
}

void testApp::updateSpeechState() {
	eSpeechState saved = state;
	
	switch(state) {
		case SPEECH_WAITING:
			if(currentPose == POSE_BOUNCING) {
				speechStart();
			}
			break;
		case SPEECH_IN_PROCESS:
			if( !inSafeArea() ) {
				speechInterrupt();
			}
			break;
		case SPEECH_INTERRUPTED:
			if( inSafeArea() ) {
				speechResume();
			}
			break;
	}
	
	lastState = saved;
}

bool testApp::inSafeArea() {
	return safetyArea.inside( (cursor.x - ofGetViewportWidth()/2), (cursor.y - ofGetViewportHeight()/2) );
}

void testApp::updateOscQueue() {

    while( receiver.hasWaitingMessages() ) {
        ofxOscMessage m;
        receiver.getNextMessage(&m);

        if(m.getAddress() == "/wiibboard/vpos"){
            // read position
						this->cursor.x = m.getArgAsFloat(0);
						this->cursor.x *= ofGetViewportWidth();
            this->cursor.y = m.getArgAsFloat(1);
						this->cursor.y *= ofGetViewportHeight();
						// read weight
            this->lastWeightMeasure = m.getArgAsFloat(2);
						this->lastWeightMeasure *= SCALE_WEIGHT;
						this->currentPose = getCurrentPose( lastWeightMeasure );
        } else {
					ofLog() << "message: " << m.getAddress() << endl;
				}
    } // while
}

//--------------------------------------------------------------
void testApp::update() {

		ofSoundUpdate();
		
    //cout << "trail.sz = " << trail.size() << endl;

    if(bMouseMode) {
        // update cursor position from mouse
        this->cursor = ofPoint(ofGetMouseX(), ofGetMouseY());
    } else {
        updateOscQueue();
    }

    if( trail.empty() ) {
        trail.push_back( ofPoint(this->cursor) ); // push first trail
    } else {
        ofPoint last = trail.back();

        // do not update if it hasn't moved
        if( this->cursor.distance(last) < 5 ) return;

        // update trail
        trail.push_back( ofPoint(this->cursor) ); // add a new one
        if(trail.size() > MAX_TRAIL_PTS)
            trail.pop_front(); // if it's too big, get rid of the oldest one
    }
		
		updateSpeechState();
}

//--------------------------------------------------------------
void testApp::draw(){
    //ofBackgroundGradient(ofColor(255, 255, 255), ofColor(200, 0, 255), OF_GRADIENT_BAR);
    ofEnableAlphaBlending();
		ofEnableSmoothing();

    float sx = 2.8;
    float sy = sx;

    float rz = ofMap(cursor.x, 0, ofGetViewportWidth(), -80, 80);

		//the value of changingRadius will be different depending on the timeline
		//float changingRadius = timeline.getKeyframeValue("MyCircleRadius");
	
    ofPushMatrix();
        ofTranslate(ofGetWidth()/2, ofGetHeight() / 2);
        ofScale(sx, sy, 0);
        ofRotateZ( rz );

        glBegin(GL_QUADS);
            glColor4fv((GLfloat*)&csky.v);
            glVertex3f(sky.x, sky.y, 0.0);

            glColor4fv((GLfloat*)&chup.v);
            glVertex3f(sky.x, sky.y+sky.height, 0.0);

            glColor4fv((GLfloat*)&chup.v);
            glVertex3f(sky.x+sky.width, sky.y+sky.height, 0.0);

            glColor4fv((GLfloat*)&csky.v);
            glVertex3f(sky.x+sky.width, sky.y, 0.0);
        glEnd();

        //ofTranslate(ofGetWidth()/2, (ofGetHeight() / 4)*3);
        //ofScale(sx, sy, 0);
        //ofRotateZ( rz );
        glBegin(GL_QUADS);
            glColor4fv((GLfloat*)&chdown.v);
            glVertex3f(ground.x, ground.y, 0.0);

            glColor4fv((GLfloat*)&cground.v);
            glVertex3f(ground.x, ground.y+ground.height, 0.0);

            glColor4fv((GLfloat*)&cground.v);
            glVertex3f(ground.x+ground.width, ground.y+ground.height, 0.0);

            glColor4fv((GLfloat*)&chdown.v);
            glVertex3f(ground.x+ground.width, ground.y, 0.0);
        glEnd();
    ofPopMatrix();

		// draw safety area
		ofPushMatrix();
			ofNoFill();
			ofColor col;
			// determine color
			if( inSafeArea() ) {
				ofSetLineWidth(2.0);
				col = ofColor(0, 0, 255);
			} else {
				ofSetLineWidth(4.0);
				col = ofColor(255, 0, 0);
			}
			ofSetColor(col);
			ofTranslate(ofGetViewportWidth()/2, ofGetViewportHeight()/2);
			ofRect(this->safetyArea.getTopLeft().x, this->safetyArea.getTopLeft().y, this->safetyArea.getWidth(), this->safetyArea.getHeight() );
		ofPopMatrix();

    // draw balance trail
    ofSetLineWidth(4.5);
    int alpha = 0;
    deque<ofPoint>::iterator it = trail.begin();
    for( int i = 0; i < trail.size(); i++) { //i += 2) {
        ofPoint o = trail[i];
/*
        ofPoint d = trail[i+1];
        ofSetColor(0, 0, 180, alpha);
        ofLine(o, d);
        cout << "trail.sz = " << trail.size() << endl;
        cout << "o.x; " << o.x << "o.y: " << o.y << endl;
        cout << "d.x; " << d.x << "d.y: " << d.y << endl;
        cout << endl;
*/

        ofSetColor(0, 0, 128, alpha);
        ofCircle(o, 6);
        ofSetColor(255, 255, 255, alpha);
        ofCircle(o, 4);
				
				/*
				// heading angle
				double a1 = atan2(object.y - player.y, object.x - player.x);
				rel_angle -= floor((rel_angle + PI)/(2*PI)) * (2*PI) - PI;
				*/
				alpha++;
    }
		ofSetLineWidth(1.0);

		// draw current slide if any
		//ofDisableAlphaBlending();
		ofSetColor(255, 255, 255, 200);
		if(currentSlide != "none") {
			float iw = slidemap[currentSlide].width;
			float ih = slidemap[currentSlide].height;
			slidemap[currentSlide].draw( 40, (ofGetViewportHeight()/2) - (ih/2) );
		}
		
		// draw debugging info
		if (bShowDebuggingInfo) {
			ofSetColor(0, 0, 0, 255);
			stringstream ss;
			ss << "pos (" << cursor.x << ", " << cursor.y << ")";
			ofDrawBitmapString(ss.str(), 20, ofGetViewportHeight() - 100);
			ss.str(std::string());
			ss <<  "gauged = " << lastWeightMeasure;
			ofDrawBitmapString(ss.str(), 20, ofGetViewportHeight() - 82);
			ss.str(std::string());
			ss <<  "pose: " << getPoseString();
			ofDrawBitmapString(ss.str(), 20, ofGetViewportHeight() - 62);
			ss.str(std::string());
			ss <<  "speech: " << getStateString();
			ofDrawBitmapString(ss.str(), 20, ofGetViewportHeight() - 42);
		}
		ofEnableAlphaBlending();
		

	ofDisableSmoothing();
	timeline.draw();
	ofDisableAlphaBlending();
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
	if (key == 'M'){
		bMouseMode = !bMouseMode;
	}

  if(key == 'E'){
		bEditMode != bEditMode;
    timeline.toggleShow();
  }
	
	if (key == 'D') {
		bShowDebuggingInfo = !bShowDebuggingInfo;
	}
	
	if (key == 'F') {
		ofToggleFullscreen();
	}

	if(key == ' '){
		//calling play on the waveform controls the timeline playback
		waveform.togglePlay();
		timeline.togglePlay();
		//bMouseMode = !bMouseMode;
	}
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){

}
